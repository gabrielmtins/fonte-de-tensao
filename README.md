# Fonte de Tensão - Projeto da Disciplinas SSC0180

## Objetivo

Construção de uma fonte capaz de transformar 127v corrente alternada em 3~12v
corrente contínua.
## Componentes

| Quantidade | Componentes             | Valor R$        |
|------------|-------------------------|-----------------|
| 4x         | Diodo 1N4001            | 0,20 * 4 = 0,80 |
| 4x         | Resistor 1k ohm         | 0,07 * 4 = 0,28 |
| 1x         | Diodo Zenner 13V 1N4743 | 0,50            |
| 1x         | Potenciômetro 5k ohm    | 7,00            |
| 1x         | Transistor 2N2222A      | 2,00            |
| 1x         | Resistor 120 ohm        | 0,07            |
| 1x         | LED                     | 0,50            |
| Total      |                         | R$ 11,15        |

Obs.: O transformador utilizado foi o transformador do laboratório.

## Cálculo do capacitor

![alt text](./calculo.png)

[Vídeo da explicação dos cálculos do projeto](https://youtu.be/K3fbNnLkzPA)

## Imagem do circuito

![alt text](./circuito_falstad.png)

Aqui está o link para o circuito no simulador [Falstad](https://falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWK0AckDMYwE4As3sA2SQgdgCYFsQFIaRd0aBTAWiwCgA3EV83OmEK9+g4lHAhxYNBLowEHACoiB4YUPXyGyMOkLYUhdJD0GjTdrFw5jBdJiwpSc6AWwcAJlNw-1-8hBPZgAzAEMAVwAbABcvf01CX1Z0QMDg8Oi47yS-LEDNdNDI2Pj8-1yUtKDirI4AdwTpUg1xSAbwFq1O1qgOsC7Ncs12gHM-XNzy+Q7K1L9WIj6AJx86SbVp8HIxtfAwQKTBXF8ZgGMQE19cq955+Vg4XBc+dGh0bA+ENHQjMEhsAhTo8wB1blVLpBkkt2o1KMJFsJKjCOKsTiheEt0bwRuB4B14ZjhOCUehfAAvZgAO2Yy3YBXA0FIYEYCHIKBw7P+cHIbBcdHIj3gwpFKF8enAHHJDFwGNy2PYwkClJpdIOHHGtxuUPAJzkGplct82JZpz6jQQXQ261OYJ1iMuhAxDvaMRoetlNC6nsEOJguCB9hkH0of2QzNl2FItEgpFlZgx3JqmVKq30zqW6f8gnxQkCWc0WdkRRTMVYUWY3gg2hgpgNRboBb1M0aWYdCpRFq6EMtCM7Xr7wg7wnaA2NS00QOu4po8GQiBAACVmABnACWK5iYSpZ2YHQVmnwCJG+6WioYE5HBqP2wxN9N+q7xMCvbugXaaIn7IvwlkAvxAD2IDkBAuR0AIBDAQ877AcBEDoBwQA)

## Imagem do esquemático

![alt text](./esquematico.jpeg)

## Imagem do PCB no EAGLE

![alt text](./pcb.png)

## Imagem do projeto na protoboard

![alt text](./protoboard.png)

[Vídeo do projeto funcionando na protoboard](https://youtu.be/7smW0eTfjk8) <br>

## Alunos:

Gabriel Martins Monteiro, NUSP: 14572099 <br>
Eduardo Pereira de Luna Freire, NUSP: 14567304 <br>
Francyelio de Jesus Campos Lima, NUSP: 13676537 <br>
Hélio Márcio Cabral Santos, NUSP: 14577862 <br>
